# 1. Modelo proposto

Este modelo permite responder às seguintes funcionalidades:

- Configuração de taxas de IVA
- Gestão de clientes
- Gestão de fornecedores
- Gestão de vendedores
- Gestão de produtos - Especificação da taxa de Iva, preço de compra e/ou preço de venda;
- Gestão de compras a fornecedores
- Gestão de vendas a clientes

Na gestão de compras e vendas existe um processo de aprovação hierarquica. O processo (de aprovação de uma ordem de venda) descreve-se de acordo com o seguinte diagrama de estados:

![](state-diagram.svg)

Na figura observa-se:

- Ao submeter uma ordem de venda, esta passa para o estado "InSalesTeamManagerReview" - Em revisão pelo gestor da equipa de vendas. Neste estado, é ainda possível ao vendedor alterar a ordem;
- Após a aceitação do gestor da equipa de vendas, dependendo do valor total da venda, a ordem passa para o estado "InSalesManagerReview" (em revisão pelo gestor de vendas) ou "InFinancialManagerReview" (em revisão pelo gestor financeiro);
- Em qualquer um dos estados de revisão, caso haja uma rejeição da ordem esta volta para o estado inicial, podendo ser alterada.

No caso das ordens de compra, o processo é similar sendo que:

- Em vez do gestor da equipa de vendas temos o gestor da equipa de compras;
- Em vez do gestor de vendas temos o gestor de compras;

# 2. Estrutura do zip de importação/exportação do modelo

Dentro do zip encontram-se a pasta **Model** que contém a definição dos conceitos de negócio, das queries e da user interface.

## 2.1 Model

Dentro da pasta Model encontram-se as definições de:

| Definição                | Linguagem | Explicação                                                                                                                                                                                                                                                                                                                   |
| ------------------------ | --------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Agent**                | Negócio   | Definições dos agentes do negócio (i.e. Cliente, Vendedor, Fornecedor)                                                                                                                                                                                                                                                       |
| **ApplicationBehaviour** | Aplicação | Contém a definição dos comportamentos de aplicação (scripts). Neste caso o modelo inclui um comportamento que faz reset ao tenant, apagando todas as entidades da modelação.                                                                                                                                                 |
| **Dashboard**            | UI        | Definição de "páginas" da user interface. Os dashboards podem incluir as listagens (List) definidas. Existe, por norma, um dashboard por entidade de negócio onde é apresentada a listagem sobre essa entidade. Neste caso existe ainda o Dashboard de reset de Tenant - explicado abaixo.                                   |
| **DataSource**           | Negócio   | Definição da origem dos dados (relevante para cenários onde os dados podem ser obtidos de diferentes sistemas e.g. ERP, API externa). Por omissão é utilizado o datasource "System" que representa a BD omnia.                                                                                                               |
| **Document**             | Negócio   | Definição dos documentos do negócio (i.e. Ordem de venda, ordem de compra). Estes são númerados de forma sequêncial e, portanto, necessitam de uma série associada.                                                                                                                                                          |
| **Enumeration**          | Negócio   | Definição das enumerações do negócio. As decisões possíveis, por estado, da máquina de estados são também consideradas enumerações e, portanto, encontram-se nesta pasta.                                                                                                                                                    |
| **Form**                 | UI        | Definição dos formulários relativos às entidades do negócio (i.e. agent, document, generic entity, resource, serie).                                                                                                                                                                                                         |
| **Generic Entity**       | Negócio   | Definição das entidades de negócio (que não são representáveis por Agents, Resources ou Documents). Estas entidades podem ser utilizadas na composição de documentos (E.g. O documento PurchaseOrder contém 0 ou mais generic entity PurchaseOrderLine ).                                                                    |
| **List**                 | UI        | Definição das listagens da user interface (cada listagem refere-se a uma query).                                                                                                                                                                                                                                             |
| **Query**                | Query     | Definição das queries de consulta de dados. Existe pelo menos uma query para cada entidade de negócio. (_Por convenção_ as queries cujo nome termina em "Adv" são definidas em SQL).                                                                                                                                         |
| **Resource**             | Negócio   | Definição dos recursos (que podem ser trocados entre agentes) do negócio. Neste caso, o Produto (c/ Preço de venda e/ou compra).                                                                                                                                                                                             |
| **Selector**             | UI        | Definição dos componentes da user interface relativos à máquina de estados de cada entidade. Em cada estado encontra-se a definição dos botões de UI com as decisões possíveis (e.g. Aceitar, Rejeitar). Existe ainda, por máquina de estados, a definição da combo-box que permite alterar o estado da entidade do negócio. |
| **Serie**                | Negócio   | Definição das séries para os documentos (necessárias para os sequenciar numericamente).                                                                                                                                                                                                                                      |
| **StateMachine**         | Negócio   | Definição da máquina de estados (i.e. estados, transições, decisões e comportamentos) para as entidades de negócio. Poderá, ou não, existir uma máquina de estados para uma entidade de negócio.                                                                                                                             |

# 3. Variabilidade

## 3.1. Feature toggling

- Desligar a gestão de vendas - As seguintes funcionalidades deixam de ser necessárias, e portanto poderão também ser desligadas:

  - Gestão de vendedores
  - Gestão de clientes
  - Atributo do preço de venda nos produtos

- Desligar a gestão de compras - As seguintes funcionalidades deixam de ser necessárias, e portanto poderão também ser desligadas:
  - Gestão de fornecedores
  - Atributo do preço de compra nos produtos

### 3.1.1. Como desligar as features

Para desligar cada uma das seguintes features é necessário remover os seguintes ficheiros do .zip do modelo:

| Feature                | Ficheiros a remover                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ---------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Gestão de vendas **\***      | Model/Document/SaleOrder.json <br/> Model/Dashboard/SaleOrderDashboard.json <br/> Model/Form/SaleOrderForm.json <br/> Model/List/SaleOrderList.json <br/> Model/Query/SaleOrderQuery.json <br/> Model/Serie/SaleOrderSerie.json <br/> Model/Dashboard/SaleOrderSerieDashboard.json <br/> Model/Form/SaleOrderSerieForm.json <br/> Model/List/SaleOrderSerieList.json <br/> Model/Query/SaleOrderSerieQuery.json <br/> Model/Enumeration/SaleOrder\*.json <br/> Model/Selector/SaleOrder\*.json <br/> Model/StateMachine/SaleOrderStateMachine.json <br/> Model/GenericEntity/SaleOrderLine.json                                                    |
| Gestão de vendedores   | Model/Agent/SalesPerson.json <br/> Model/Dashboard/SalesPersonDashboard.json <br/> Model/Form/SalesPersonForm.json <br/> Model/List/SalesPersonList.json <br/> Model/Query/SalesPersonQuery.json                                                                                                                                                                                                                                                                                                                                                                                                       |
| Gestão de clientes     | Model/Agent/Client.json <br/> Model/Dashboard/ClientDashboard.json <br/> Model/Form/ClientForm.json <br/> Model/List/ClientList.json <br/> Model/Query/ClientQuery.json                                                                                                                                                                                                                                                                                                                                                                                                                                |
| Gestão de compras **\***     | Model/Document/PurchaseOrder.json <br/> Model/Dashboard/PurchaseOrderDashboard.json <br/> Model/Form/PurchaseOrderForm.json <br/> Model/List/PurchaseOrderList.json <br/> Model/Query/PurchaseOrderQuery.json <br/> Model/Serie/PurchaseOrderSerie.json <br/> Model/Dashboard/PurchaseOrderSerieDashboard.json <br/> Model/Form/PurchaseOrderSerieForm.json <br/> Model/List/PurchaseOrderSerieList.json <br/> Model/Query/PurchaseOrderSerieQuery.json <br/> Model/Enumeration/PurchaseOrder\*.json <br/> Model/Selector/PurchaseOrder\*.json <br/> Model/StateMachine/PurchaseOrderStateMachine.json <br/> Model/GenericEntity/PurchaseOrderLine.json|
| Gestão de fornecedores | Model/Agent/Supplier.json <br/> Model/Dashboard/SuppliertDashboard.json <br/> Model/Form/SupplierForm.json <br/> Model/List/SupplierList.json <br/> Model/Query/SupplierQuery.json                                                                                                                                                                                                                                                                                                                                                                                                                     |

**\*** Para remover o preço de compra e/ou venda do produto, os seguintes ficheiros devem ser alterados:
- Model/Resource/Product.json - Remover da lista de "attributes" o atributo com o nome "SalePrice" ou "PurchasePrice";
- Model/Query/ProductQuery.json - Remover da lista de "properties" a propriedade com o path "salePrice" ou "purchasePrice";
- Model/Form/ProductForm.json - Remover da lista de "elements" o elemento com o nome "SalePrice" ou "PurchasePrice".

## 3.2. Parametrização

Para alterar o valor limite a partir do qual a aprovação hierarquica do gestor da equipa de vendas é necessária, alterar os seguintes ficherios:
- Model/StateMachine/SaleOrderStateMachine.json - No estado "InSalesTeamManagerReview", dentro das suas transições, alterar as expressões e.g. `return this.TotalWithVat >= 1000;` para o valor desejado.

Para o caso das compras:
- Model/StateMachine/PurchaseOrderStateMachine.json - No estado "InPurchaseTeamManagerReview", dentro das suas transições, alterar as expressões e.g. `return this.TotalWithVat >= 1000;` para o valor desejado.

## 3.3. Desligar etapas de revisão hierarquica

Para remover, por exempo, a etapa de revisão pelo gestor da equipa de vendas (InSalesTeamManagerReview), deverão ser removidos os seguintes ficheiros:
- Model/Enumeration/SaleOrderStateMachineInSalesTeamManagerReviewDecisions.json
- Model/Selector/SaleOrderStateMachineInSalesTeamManagerReviewDecisionsSelector.json

Para além da remoção dos ficheiros acima referidos, é necessário remover este estado dos ficheiros:
- Model/Enumeration/SaleOrderStateMachineStates.json
- Model/Selector/SaleOrderStateMachineStatesSelector.json

Será ainda necessário rever as transições da máquina de estados da ordem de compra. Deverão ser removidas as referências ao estado apagado, sendo que deverá também ser garantido um caminho para o(s) estado(s) seguinte(s). Para efetuar esta revisão, o ficheiro Model/StateMachine/SaleOrderStateMachine.json deverá ser alterado da seguinte forma:
- Remover, da lista "states", o estado com o nome "InSalesTeamManagerReview";
- Verificar, na lista "states" os estados cujas transições apontam para o "InSalesTeamManagerReview". Nestes casos, deverá ser alterado o atributo "to" da transição para o estado seguinte.

# 4. Como importar e gerar aplicação?

1. Aceder ao https://platform.omnialowcode.com ;
2. Aceder à area de modelação do Tenant atribuído;
3. Expandir as opções do botão "Build & Deploy" e selecionar "Import". Escolher o ficheiro .zip com o modelo;
4. Esperar pela mensagem de sucesso de importação do modelo. Após isto é possível verificar, através da interface de modelação, que tudo foi importado corretamente;
5. Expandir as opções do botão "Build & Deploy" e selecionar "Clean & Build";
6. Aceder à aplicação e verificar que a mesma funciona de acordo com o modelo importado.

# 5. Como limpar o tenant?

De modo a limpar o tenant (removendo as entidades modeladas) para permitir uma nova importação de um modelo diferente, seguir os seguintes passos:

1. Aceder à aplicação;
2. No menu lateral, selecionar "Dashboards" e depois "RESET TENANT";
3. Aparecerá uma janela de confirmação na qual deverá selecionar "OK".

**Nota**: O processo de limpeza do tenant é assincrono e poderá demorar entre 1 a 2 minutos. Deverá verificar que todas as entidades modeladas foram apagadas.

Para informação adicional poderá consultar [a nossa documentação](https://docs.omnialowcode.com/).
