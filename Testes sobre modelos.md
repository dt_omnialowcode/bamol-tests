# Testes sobre modelos



## 1. Entidades: Formulários

Para cada entidade, é gerado automaticamente um formulário, com uma proposta de organização dos atributos.

Não é possível a criação de novos formulários manualmente.

### Acesso a formulários
O formulário é usado para inserção e edição de dados, e pode ser acedido através do menu lateral, ou diretamente via URL. 

O URL de um formulário é distinto entre operações de criação e de edição.

- Criação

        https://[UrlBase]/[CódigoTenant]/[Ambiente]/[TipoEntidade]/[FonteDados]/Create

        Exemplo: https://platform.omnialowcode.com/OrderMng_Tests/PRD/SaleOrder/Default/Create

- Edição

        https://[UrlBase]/[CódigoTenant]/[Ambiente]/[TipoEntidade]/[FonteDados]/Edit/[Entidade]

        Exemplo: https://platform.omnialowcode.com/OrderMng_Tests/PRD/SaleOrder/Edit/A10

Parâmetros do URL:

| Parâmetro | Descrição                                         |
| ------------------------ |  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| UrlBase                | URL base da subscrição Omnia                                              |
| CódigoTenant        | Identificador do tenant. Um tenant é uma área isolada (modelo e dados) dentro da subscrição                                        |
| Ambiente        | Ambiente do tenant. Valor a inserir: _PRD_                                         |
| TipoEntidade        | Nome do tipo de entidade modelado                                        |
| FonteDados        | Fonte dos dados. Quando são dados internos ao Omnia, o valor a inserir é _Default_                                        |
| Entidade        | O código inserido na  entidade (dados). Apenas usado em edições.                                        |

### Atributos

Um formulário é composto por um conjunto de atributos, que mapeiam diretamente para a definição da entidade no modelo. 
Um atributo é incluido no formulário sobre a forma de um elemento HTML, podendo ser de tipos simples (texto, numérico, booleano, etc.) ou de tipos complexos, como coleções. 

Tipos de atributos:

| Tipo | Descrição                                         |
| ------------------------ |  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Primitivo                | Atributo de tipo de dados simples, representado por um input adequado às especificidades do tipo                                              |
| Enumerado        | Atributo que representa um conjunto limitado de valores possíveis, definidos no modelo. Representado sobre a forma de uma _select box_                                        |
| Referência        | Atributos que representam uma relação com outra entidade do modelo. Representados sobre a forma de um atributo de texto com possibilidade de listagem e seleção de valores.                                      |
| Coleção        | Os atributos do tipo coleção representam uma relação 1-N com outra entidade complexa (Commitment, Event ou Generic Entity).São representadas numa estrutura tabular, com elementos HTML do tipo _table_.                                        |



Todos os elementos são identificados por um ID com a seguinte estrutura:

                                form-field-nomeAtributo

Exemplos de IDs possíveis:

| Atributo                | ID | Explicação                                        |
| ------------------------ | --------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| _code                | form-field-_code   | ID de um atributo base                                              |
| employee                | form-field-employee   | ID de um atributo não-base (adicionado pelo modelador)                                        |
| Collection > unitPrice                | form-field-_collection_0unitprice   | ID de um atributo que faz parte de uma coleção. Neste exemplo, o atributo _UnitPrice_ encontra-se na linha 0 da coleção _Collection_                                      |


Para efetuar operações nos formulários, são disponibilizados um conjunto de botões. Exemplos de IDs possíveis:

| Botão                | ID | Explicação                                        |
| ------------------------ | --------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Gravação da entidade                | element-form-save   |                                                 |
| Adição de linha a coleção                | element-editable-list-add-line   | Opção disponível no footer de cada coleção                                           |

## 2. Entidades: Dashboards

Para cada tipo de entidade é gerado automaticamente um dashboard, com uma lista que espelha os dados inseridos para a entidade.

É possível editar dashboards e adicionar novos elementos aos mesmos.

### Acesso a dashboards
Os dashboards podem ser acedidos através do menu lateral, ou diretamente via URL. 

O URL de um dashboard é composto da seguinte forma:

    https://[UrlBase]/[CódigoTenant]/[Ambiente]/Dashboard/[CódigoDashboard]

    Exemplo: https://platform.omnialowcode.com/OrderMng_Tests/PRD/Dashboard/SaleOrderDashboard

### Elementos

Os elementos HTML correspondentes às listas disponíveis nos dashboards são identificados diretamente pelo nome dado à lista. Por exemplo, se adicionar ao dashboard uma lista _SaleOrderList_, o elemento html terá o ID _saleorderlist_.


Para efetuar operações nos dashboards são disponibilizados um conjunto de botões. Exemplos de IDs possíveis:

| Botão                | ID | Explicação                                        |
| ------------------------ | --------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Refresh                | element-dashboard-btn-refresh   |                                                 |
| Navegar para ecrã de criação de nova entidade                | element-dashboard-btn-addnew   |                              